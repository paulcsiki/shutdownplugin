﻿using System.Globalization;
using MM.Monitor.Client;

namespace PaulCsiki.ShutdownPlugin
{
    internal class Configuration
    {
        private readonly IDataStore dataStore;

        public Configuration(IDataStore dataStore)
        {
            this.dataStore = dataStore;
        }

        public bool Enabled
        {
            get { return dataStore.GetValueForKey("enabled") == "1"; }
            set { dataStore.SetValueForKey("enabled", value ? "1" : "0"); }
        }
        public bool EveryDay
        {
            get { return dataStore.GetValueForKey("everyday") == "1"; }
            set { dataStore.SetValueForKey("everyday", value ? "1" : "0"); }
        }
        public bool WeekDays
        {
            get { return dataStore.GetValueForKey("weekdays") == "1"; }
            set { dataStore.SetValueForKey("weekdays", value ? "1" : "0"); }
        }
        public int Hour
        {
            get
            {
                try
                {
                    return int.Parse(dataStore.GetValueForKey("hour"));
                }
                catch
                {
                    return 0;
                }
            }
            set { dataStore.SetValueForKey("hour", value.ToString(CultureInfo.InvariantCulture)); }
        }
        public int Minute
        {
            get
            {
                try
                {
                    return int.Parse(dataStore.GetValueForKey("minute"));
                }
                catch
                {
                    return 0;
                }
            }
            set { dataStore.SetValueForKey("minute", value.ToString(CultureInfo.InvariantCulture)); }
        }
        public bool Force
        {
            get { return dataStore.GetValueForKey("force") == "1"; }
            set { dataStore.SetValueForKey("force", value ? "1" : "0"); }
        }
        public bool Shutdown
        {
            get { return dataStore.GetValueForKey("shutdown") == "1"; }
            set { dataStore.SetValueForKey("shutdown", value ? "1" : "0"); }
        }
        public bool Restart
        {
            get { return dataStore.GetValueForKey("restart") == "1"; }
            set { dataStore.SetValueForKey("restart", value ? "1" : "0"); }
        }
    }
}