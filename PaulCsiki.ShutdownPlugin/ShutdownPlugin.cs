﻿using System;
using System.Diagnostics;
using System.Timers;
using MM.Monitor.Client;
using DateTime = System.DateTime;
using Version = MM.Monitor.Client.Version;

namespace PaulCsiki.ShutdownPlugin
{
    public class ShutdownPlugin : ClientPlugin
    {
        private const int PAGE_ID = 1;
        private const string PICK_LIST_DAYS_ID = "days";
        private const int PICK_LIST_ITEM_DAILY_ID = 1;
        private const int PICK_LIST_ITEM_WORK_DAYS_ID = 2;
        private const string PICK_LIST_ACTION_ID = "action";
        private const int PICK_LIST_ITEM_SHUTDOWN_ID = 3;
        private const int PICK_LIST_ITEM_RESTART_ID = 4;
        private const string TIME_ID = "time";
        private const int COMMAND_ENABLE_DISABLE = 1;
        private const int COMMAND_FORCE = 2;
        private const int COMMAND_ENABLE_ONE_TIME = 3;
        private readonly Configuration configuration;
        private readonly Timer checkTimer = new Timer(1*60*1000); // Check every minute
        private DateTime? nextShutdownTime;
        private DateTime lastTrace;
        private bool oneTime;

        public ShutdownPlugin()
        {
            configuration = new Configuration(this);
            checkTimer.Elapsed += CheckAndShutdown;
        }

        public override void PluginLoaded()
        {
            SetShutdownTime();
        }
        public override string GetPluginName()
        {
            return "Shutdown Plugin";
        }
        public override string GetPluginDescription()
        {
            return "Auto shutdown plugin for Pulseway.";
        }
        public override Version GetPluginVersion()
        {
            return new Version(1, 3);
        }
        public override Groups GetAdditionalComputerDetails()
        {
            Groups container = new Groups();
            Group plugin = new Group("Shutdown Plugin");
            plugin.Items.Add(new PageItem(PAGE_ID, "Open Shutdown Plugin"));
            container.Add(plugin);
            return container;
        }
        public override Groups GetPageDetails(int pageId)
        {
            Groups container = new Groups();
            Group settings = new Group("Schedule");

            bool enabled = configuration.Enabled;
            settings.Items.Add(new SimpleItem(enabled ? "Enabled (scheduler active)" : "Disabled", "State", enabled ? SimpleItemStyle.ERROR : SimpleItemStyle.INFORMATION));

            bool everyday = configuration.EveryDay;
            bool weekdays = configuration.WeekDays;
            if (!everyday && !weekdays)
            {
                everyday = configuration.EveryDay = true;
            }
            PickListItem dailyItem = new PickListItem(PICK_LIST_ITEM_DAILY_ID, "Daily");
            PickListItem workDaysItem = new PickListItem(PICK_LIST_ITEM_WORK_DAYS_ID, "Work Days");
            PickListItems occurenceItems = new PickListItems();
            occurenceItems.Add(dailyItem);
            occurenceItems.Add(workDaysItem);
            settings.Items.Add(new PickListInputItem(PICK_LIST_DAYS_ID, everyday ? "Daily" : "Week Days", "Interval", occurenceItems, everyday ? dailyItem : workDaysItem));

            settings.Items.Add(new TimeInputItem(TIME_ID, string.Format("{0}:{1}", configuration.Hour, configuration.Minute), "Time (24 hour format)", new Time(configuration.Hour, configuration.Minute)));

            bool shutdown = configuration.Shutdown;
            bool restart = configuration.Restart;
            if (!shutdown && !restart)
            {
                shutdown = configuration.Shutdown = true;
            }
            PickListItem shutdownItem = new PickListItem(PICK_LIST_ITEM_SHUTDOWN_ID, "Shutdown");
            PickListItem restartItem = new PickListItem(PICK_LIST_ITEM_RESTART_ID, "Restart");
            PickListItems actionItems = new PickListItems();
            actionItems.Add(shutdownItem);
            actionItems.Add(restartItem);
            settings.Items.Add(new PickListInputItem(PICK_LIST_ACTION_ID, shutdown ? "Shutdown" : "Restart", "Action", actionItems, shutdown ? shutdownItem : restartItem));

            settings.Items.Add(new CommandItem(COMMAND_FORCE, "Force command", configuration.Force ? "Yes" : "No"));            

            Group commands = new Group("Commands");
            commands.Items.Add(new CommandItem(COMMAND_ENABLE_DISABLE, configuration.Enabled ? "Disable" : "Enable"));

            bool oneTimeStarted = !configuration.Enabled && oneTime;
            commands.Items.Add(new CommandItem(COMMAND_ENABLE_ONE_TIME, oneTimeStarted ? "Stop" : "Start (once)"));

            container.Add(settings);

            if ((configuration.Enabled || oneTime) && nextShutdownTime != null)
            {
                Group status = new Group("Status");
                status.Items.Add(new SimpleItem(ParseTimespan(nextShutdownTime.Value - DateTime.Now), "Time left"));
                container.Add(status);
            }

            container.Add(commands);
            return container;
        }
        public override void PageCommandReceived(int pageId, int commandId)
        {
            switch (commandId)
            {
                case COMMAND_ENABLE_DISABLE:
                    bool enabled = !configuration.Enabled;
                    checkTimer.Enabled = configuration.Enabled = enabled;
                    if (enabled)
                    {
                        SetShutdownTime();
                    }
                    else
                    {
                        nextShutdownTime = null;
                    }
                    break;
                case COMMAND_FORCE:
                    configuration.Force = !configuration.Force;
                    break;
                case COMMAND_ENABLE_ONE_TIME:
                    bool currentOneTime = !configuration.Enabled && oneTime;
                    bool newValue = oneTime = !currentOneTime;

                    configuration.Enabled = false;
                    checkTimer.Enabled = newValue;
                    if (newValue)
                    {
                        SetShutdownTime();
                    }
                    else
                    {
                        nextShutdownTime = null;
                    }
                    break;
            }
        }
        public override void TimeInputValueChanged(string inputId, Time inputValue)
        {
            configuration.Hour = inputValue.Hour;
            configuration.Minute = inputValue.Minute;

            if (configuration.Enabled || oneTime)
            {
                SetShutdownTime();
            }
        }
        public override void PickListInputValueChanged(string inputId, int pickListItemId)
        {
            if (inputId == PICK_LIST_DAYS_ID)
            {
                if (pickListItemId == PICK_LIST_ITEM_DAILY_ID)
                {
                    configuration.EveryDay = true;
                    configuration.WeekDays = false;
                }
                else
                {
                    configuration.EveryDay = false;
                    configuration.WeekDays = true;
                }
            }
            else
            {
                if (pickListItemId == PICK_LIST_ITEM_SHUTDOWN_ID)
                {
                    configuration.Shutdown = true;
                    configuration.Restart = false;
                }
                else
                {
                    configuration.Shutdown = false;
                    configuration.Restart = true;
                }
            }

            if (configuration.Enabled || oneTime)
            {
                SetShutdownTime();
            }
        }

        private void SetShutdownTime()
        {
            DateTime now = DateTime.Now;
            if (configuration.EveryDay)
            {
                DateTime today = new DateTime(now.Year, now.Month, now.Day, configuration.Hour, configuration.Minute, 0);
                if ((now - today).TotalSeconds < 0) // scheduled time is in the future
                {
                    nextShutdownTime = today;
                }
                else // we passed the scheduled time so let's schedule it for tomorrow
                {
                    DateTime tomorrow = now.AddDays(1);
                    nextShutdownTime = new DateTime(tomorrow.Year, tomorrow.Month, tomorrow.Day, configuration.Hour, configuration.Minute, 0);
                }
            }
            else
            {
                DateTime today = new DateTime(now.Year, now.Month, now.Day, configuration.Hour, configuration.Minute, 0);
                if ((now - today).TotalSeconds < 0 && IsWeekDay(today.DayOfWeek)) // scheduled time is in the future
                {
                    nextShutdownTime = today;
                }
                else
                {
                    DateTime nextDay = today;
                    while (true)
                    {
                        nextDay = nextDay.AddDays(1);
                        if (IsWeekDay(nextDay.DayOfWeek))
                        {
                            nextShutdownTime = new DateTime(nextDay.Year, nextDay.Month, nextDay.Day, configuration.Hour, configuration.Minute, 0);
                            break;
                        }
                    }
                }
            }

            Trace(string.Format("Set shutdown time to: {0}.", nextShutdownTime.Value.ToString("F")));
        }
        private void CheckAndShutdown(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            checkTimer.Stop();

            try
            {
                if (nextShutdownTime == null)
                {
                    Trace("No next shutdown time, stopping checks...");
                    return;
                }

                TimeSpan difference = nextShutdownTime.Value - DateTime.Now;
                if (difference.TotalSeconds > 0)
                {
                    if ((DateTime.Now - lastTrace).TotalMinutes >= 10)
                    {
                        Trace(string.Format("Will shutdown in: {0}.", ParseTimespan(difference)));
                        lastTrace = DateTime.Now;
                    }
                }
                else
                {
                    if (!configuration.Enabled && !oneTime)
                    {
                        Trace("Shutdown schedule is no longer enabled, aborting shutdown...");
                        return;
                    }
                    Trace("Shutting down...");
                    Process process = new Process();
                    process.StartInfo.FileName = "shutdown.exe";
                    string arguments = configuration.Shutdown ? "-s" : "-r";
                    if (configuration.Force)
                    {
                        arguments += " -f";
                    }
                    arguments += " -t 30"; // allow 30 seconds before shutting down
                    process.StartInfo.Arguments = arguments;
                    process.StartInfo.UseShellExecute = true;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    bool start = process.Start();
                    string actionName = configuration.Shutdown ? "Shutdown" : "Restart";

                    Trace(start ? actionName + " successfully started." : actionName + " failed to execute.");
                    return;
                }
            }
            catch (Exception ex)
            {
                Trace(ex.ToString());
            }

            if (configuration.Enabled || oneTime)
            {
                checkTimer.Start();
            }
        }

        private static bool IsWeekDay(DayOfWeek day)
        {
            switch (day)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Friday:
                    return true;
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException("day");
            }
        }
        private static string ParseTimespan(TimeSpan span)
        {
            string duration = "";
            if (span.Days > 0)
            {
                duration += span.Days + " day" + (span.Days == 1 ? "" : "s");
            }
            if (span.Hours > 0)
            {
                if (!string.IsNullOrWhiteSpace(duration))
                {
                    duration += " and ";
                }
                duration += span.Hours + " hour" + (span.Hours == 1 ? "" : "s");
            }
            if (span.Minutes > 0)
            {
                if (!string.IsNullOrWhiteSpace(duration))
                {
                    if (duration.Contains(" and "))
                    {
                        duration = duration.Replace(" and ", ", ");
                    }
                    if (!duration.EndsWith(" "))
                    {
                        duration += " ";
                    }
                    duration += "and ";
                }
                duration += span.Minutes + " minute" + (span.Minutes == 1 ? "" : "s");
            }
            if (span.Seconds > 0)
            {
                if (!string.IsNullOrWhiteSpace(duration))
                {
                    if (duration.Contains(" and "))
                    {
                        duration = duration.Replace(" and ", ", ");
                    }
                    if (!duration.EndsWith(" "))
                    {
                        duration += " ";
                    }
                    duration += "and ";
                }
                duration += span.Seconds + " second" + (span.Seconds == 1 ? "" : "s");
            }
            return string.IsNullOrEmpty(duration) ? span.ToString() : duration;
        }
    }
}