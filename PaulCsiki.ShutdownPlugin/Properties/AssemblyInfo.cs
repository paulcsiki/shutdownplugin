﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("PaulCsiki.ShutdownPlugin")]
[assembly: AssemblyDescription("Auto shutdown plugin for Pulseway (www.pulseway.com).")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Paul Csiki")]
[assembly: AssemblyProduct("ShutdownPlugin")]
[assembly: AssemblyCopyright("Copyright © Paul Csiki 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("8c795ccc-1248-4e67-bc5d-2fcc60506808")]
[assembly: AssemblyVersion("1.3.0.*")]
[assembly: AssemblyFileVersion("1.3.0.0")]