# PaulCsiki.ShutdownPlugin #

A simple shutdown scheduler plugin for Pulseway ([www.pulseway.com](www.pulseway.com)).

### Installation instructions ###

* Download or compile latest version
* Copy the PaulCsiki.ShutdownPlugin.dll file to Pulseway's installation directory
* Register the plugin with Pulseway Manager (Plugins tab)